const sum = require('../sum.js');

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2,"sumar")).toBe(3);
});

test('adds 5 + 4 to equal 9', () => {
  expect(sum(5, 4,"sumar")).toBe(9);
});

test('adds 18 + 0 to equal 18', () => {
  expect(sum(18, 0,"sumar")).toBe(18);
});


test('adds 0 + 1 to equal 18', () => {
  expect(sum(0, 1,"sumar")).toBe(1);
});

test('remove 25 - 21 to equal 18', () => {
  expect(sum(25, 21,"restar")).toBe(4);
});


test('remove 1 - 1 to equal 0', () => {
  expect(sum(1, 1,"restar")).toBe(0);
});


test('remove 4 - 1 to equal 3', () => {
  expect(sum(4, 1,"restar")).toBe(3);
});


test('remove 10 - 3 to equal 7', () => {
  expect(sum(10, 3,"restar")).toBe(7);
});